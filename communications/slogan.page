---
title: Slogans / Catchphrases / Elevator Speeches
categories: communications
...

Ideas with explanations of how to present parts of the system as briefly as possible:

## Primary Slogan

> **Crowdmatching for public goods**

That primary slogan captures the ideas of matching, bringing everyone together, cooperation, and the scope of projects we're focused on. Although it doesn't specify FLO, that's implied with public goods. The co-op angle is covered by our name. The only real missing element is sustainability / long-term support.

## Description

A summary paragraph for things like applying for a booth at a conference or for press etc:

Snowdrift.coop funds public goods through our sustainable crowdmatching system. Each patron of a project makes a monthly donation of a tiny base amount multiplied by the number of patrons who donate together. We'll include software, art, research, journalism, educational resources… any project that produces public goods under free/libre/open terms. The platform itself will run as a cooperative with membership open to all participants.

## Simple overview statements

In an elevator speech, our key take-aways are:

* Public goods, particularly FLO digital works (*not* just programs: art/education/journalism/research…)
* Crowdmatching, network effect
* Long-term livable salaries, sustainability
* Community = patron (like old days of patronage)
    * For some audiences, it's community = "client"
    * With community as patron, projects should serve the public interest — so no anti-features / damaged goods
* Co-op
* You set a monthly budget limit

Another angle: **the path-of-least-resistance online today currently offers lots of features but lots of compromises for privacy and freedom and otherwise**. We need to clear a path that leads to a better future.

### Old slogans

Our slogan evolved over time:

* "Working together to clear our obstacles"
* "Working together to clear the path to a free/libre/open world"
* "Clearing the path to a free/libre/open world"
* "Free the commons"
* "Crowdmatching for public goods"

### Useful short sentences

* “I'll donate more if more people will join me.”

* Long-term community patronage, not one-off campaigns.

* Our crowdmatching system focuses community funding on *consensus* projects.

* Crowdmatching is about growing the patronage community

* Public broadcasting commonly uses matching donations and "sustaining member" pledges… We combine those into one approach where everyone matches everyone else — all specifically tailored for FLO projects.

## Comparison to other sites

We combine sustainable patronage *with* a mutual-assurance contract. No other system does that.

* Like Patreon, we help facilitate community of patrons around creative projects
* Like Kickstarter, we provide assurance that you don't go it alone
* Also like Kickstarter, we serve as a test of community support
    * But instead of artificial all-or-nothing / make-or-break, we're flexible
* Like Flattr, we do regular microdonations (with a system-wide budget, but it's not fixed spend-all and we hope people will *increase* budgets as they see the value, so it's not a strict zero-sum game like Flattr is)
* Like Liberapay, we work to give people a reliable living and don't take a cut of the donations
* Like (now-defunct) PledgeBank (and like Wikimedia's small-donor focus), we care about the number of participants, not just the amount of money
* We don't tie funding to exact work like bounty sites do, but ongoing funding gives patrons a way to hold projects accountable for progress over time. Projects can also connect requests with patronage to be sure to honor the requests of the patrons.
* We focus on exclusively FLO projects
    * Most sites fund proprietary projects with only a few software-focused exceptions like Bountysource 

## For game-theory wonks

1. The prisoner's dilemma encourages deviation even though everyone would be better off cooperating.
2. The snowdrift dilemma changes the rules a bit and we get more likely cooperation but can fall into a waiting game.[^snowdrift]
3. Crowdmatching tweaks the game to *maximize* the payoff and *minimize* the risk when choosing to cooperate
    * Two solutions to dilemmas: iteration and assurance contract — we use *both* at once (and nobody else does that).

[^snowdrift]: Note that *snowdrift* brings up two possible metaphors: (A) the obstacle to cooperatively clear as in the snowdrift dilemma or (B) how microdonations add up like snowflakes adding up to large snowdrifts. We should *avoid* the latter interpretation so we don't get a screwy mixed-metaphor.

## For projects to say to supporters

*See our separate suggestions for projects to use on their [external pages](external).*