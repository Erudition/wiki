<section class="wikiNav">
    <form action="$base$/_search" method="get" id="searchform">
        <input id="patterns" type="text" name="patterns" />
        <button id="search" type="submit" name="search" />Go</button>
    </form>
    <ul>
        <li><a href="$base$/_index">All pages</a></li>
        <li><a href="$base$/_categories">Categories</a></li>
        <li><a href="$base$/_activity">Recent activity</a></li>
        $if(wikiupload)$
            <li><a href="$base$/_upload">Upload a file</a></li>
        $endif$
        <li><a href="$base$/Help">Help</a></li>
    </ul>
</section>
