<ul class="main">
    <li>
        <a class="logo" href="https://snowdrift.coop" alt="Snowdrift.coop" 
        title="Go to main site"><img src="$base$/img/logo-small.png" /></a>
    </li>
</ul>
<ul class="right">
    <li>
        <noscript>
            <a href="$base$/_login">Log In</a>
            <a href="$base$/_logout">Log Out</a>
        </noscript>
        &nbsp;
        <a class="login" id="loginlink" href="$base$/_login">Log In</a>
        <a class="login" id="logoutlink" href="$base$/_logout">Log Out 
        <span id="logged_in_user"></span></a>
    </li>
</ul>

