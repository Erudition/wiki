# Snowdrift.coop Board Meeting — September 13, 2018

Attendees: Aaron, Charles, Erik, Stephen

## Legal situation background

- Aaron: Primary purpose of the initial board is to specify/ratify the bylaws; once that's in place, we'll have the first election to replace the appointed board
- Aaron: How we got to where we are, what decisions going forward
- Aaron: Steering committee, various people involved over time. It dwindled as we got a more active team with fewer comittee check-ins needed
- Aaron: original proposal was to go with a multi-stakeholder coop, patrons/projects/employees as 3 recognized classes
- Erik: Most of my career has been spent in the 501c3
- Erik: what level of legal advice has there been to date?
- Erik: the primary motivation we had with a multi-stakeholder cooperative was the principle that people being left out of governance leads to problems.
- Aaron: after drafting bylaws around that, we went to a co-op lawyer in michigan, who asked us to fill out a rubrick instead of bringing her an already-drafted copy of the bylaws, which would be
- Aaron: ie, she wants the clearest/simplest/shortest way to explain our values and intentions
-  Charles: plain english input, she'll turn it into legal output
- Aaron: basically yes (could be simpler than plain english)
- Erik: okay with being domiciled in michigan?
- Aaron: point was, lawyer knew michigan coop law
- Aaron: due to costs of her time, we've minimized how much we go back to her over the last few years
- Aaron: we've pulled back from a lot of our initial scope, especially pulling back from the multi-stakeholder part, because our core mission is actually about providing freedom for end users, the general public; it's incidental that we want creators to be paid well
- Charles: Is this a decision that needs to be 
- Aaron: It's possible to change later, but easier 
- Aaron: lawyer filed articles of incorporation in the state of michigan; I am the sole signer. We will need to file updated ones.
- Aaron: responsibility of board is to get our initial
- Aaron: single stakeholder is easier/less costly.
- Aaron: Proposal at this point: you become a member of snowdrift.coop by becoming a patron of the snowdrift.coop project.
- Erik: I like the simplicity of the legal status there; nobody reads the fine print, so to whatever extent the lived reality mirrors the legal docs, the better people will understand it.
- Aaron: In michigan, it's possible under consumer coop law to be designated as a non-profit as well as a coop; that has a signaling effect; it's nice to be able to say we're a non-profit — our mission is our only goal
- Erik: Are you still committed to being registered in the state of michigan?
- Aaron: no
- Charles: It is rare for coops to be allowed to be multistakeholder
- Erik: Is anyone involved in the project based in michigan at this point?
- Aaron: only our lawyer, whose address is our legal address
- Erik: A little concerned about single point of failure there
- Charles: Doing business in 50 states and being a money service business, what are the issues there?
- Aaron: many issues there. It will take several meetings and I hope to bring others on board.
- Aaron: no liability issues, incorporation is just like any other non-profit; like an llc, people aren't independently liable (not any more than an llc)
- Charles: fcc has requirements about money transfer…
- Aaron: the only safe way to go is not hold money at all. In this case our plan is to work with stripe
- Charles: securities still has rules about that...
- Aaron: yes, and stripe deals with those. They *do* care about not giving money to unsavory persons
- Charles: so, striple handles approval?
- Aaron: We still need to screen projects. We're not going to just let anyone sign up.
- Aaron: gittip/gratipay is an example of doing this the wrong way and running into tons of issues
- Erik: is there something we can do to help move this along?
- Aaron: There's a number of things. One is helping figure out who else to bring on the board! I have a list of potential candidates, but could use outside advice on this process.
- Aaron: I'm interested in trying to get a diverse set of people
- Charles: You mentioned having a board section on discourse? I'd like to do this async; I think I can do a better job that way.
- Aaron: That will be a very good tool, probably the primary place to collaborate
- Charles: a lot of things, I'm public being transparent
- Aaron: we actually do have public project management and legal sections, in addition to the private board area
- Aaron: one last thing we've been working on 
- Aaron: we don't have a laywer on hand who knows about crowdfunding stuff
- Aaron: our current lawyer knows about coop stuff, but we need other legal assistance clarifying the money aspects
- Charles: need an evaluation criteria <for ???>
- Aaron: related — we need to figure out project requirements. There's a legal issue in terms of which projects we allow on the site, but also other requirements for projects, eg, it'll initially be easier to deal with projects in the US (although to some degreee, any project recognized by stripe will be good)

- Charles: for clarity in meeting notes, questions I asked earlier were:
    1. Snowdrift status as a money service business
    2. Applicability of federal securities statutes to crowdfunding platforms

- Erik: I'll monitor that section of the forum for RFCs, as far as future metings go 
- Aaron: other areas of the project have moved forward in fits and starts; this is one that hasn't moved much so I'm excited that it's moving now.

---

*At this point, Erik had to go; Aaron and Charles stuck around to chat informally. At some point we decided to start taking notes again.*

## Post-meeting

Charles: next step is to plan compliance committee of the Board, Board composition etc.

- Aaron: when we go to our lawyer with our political goals, then she'll give us our legal requirements
- Aaron: I tend to be very democratic and don't want a benevolent dictator type situation.
- Aaron: The only defining factor for who can participate in the coop is patronage of snowdrift.coop
- Aaron: It amounts to me in consultation with the team and outside advisors formally (ie, you)

- Charles: I presume you want me wearing my governance hat strictly, not management/consultant hat
- Charles: governance -> abstract, delegating to you.
- Aaron: in general, input welcome anywhere. Within the board meeting, governance takes priority, although other stuff isn't out of bounds
- Charles: ok, will restrict other stuff to 1-on-1's
- Aaron: once governance is up to speed, I welcome the board to also weigh in on other types of issues