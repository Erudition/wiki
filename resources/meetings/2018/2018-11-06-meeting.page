<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2018/ -->

# Meeting — November 6, 2018

Attendees: wolftune, smichel17, msiep, Salt

<!-- Check-in round -->

## Carry-overs from last meeting

```
## Meeting effectiveness & reliability of reaching people
- NEXT STEP: Try https://meet.jit.si/snowdrift next week. If it goes well, update discoure post and wiki.

## tracking ops tasks for just server maintenance
- NEXT STEP (chreekat): Review/capture ops priorities [chreekat, in his own list]

## Bryan/Stephen meeting — CI/CD improvements
- NEXT STEP (chreekat): write blog post about this process, document <https://git.snowdrift.coop/sd/snowdrift/issues/111>

## Governance overhaul / Volunteer recruitment
- NEXT STEP (chreekat): Decide whether we want to open up option of moving off of haskell for our web framework.
- NEXT STEP (wolftune): posts on discourse about things he has personal notes on governance roles.
- NEXT STEP (wolftune): co-working to create a more complete list of roles/titles we want (capture wolftune's notes in better form)

## Project outreach, Kodi etc
- NEXT STEP (Salt): Set up an hour to co-work with Michael
- NEXT STEP (msiep): work on feedback form <https://git.snowdrift.coop/sd/outreach/issues/41>

## CFPs
- NEXT STEP (Salt): Prepare CFP stuff, get feedback, check in at future meetings
```

## Metrics

Discourse (over past week):

- Signups: 3
- New Topics: 16
- Posts: 52

Sociocracy:

- Page 108

<!-- New Agenda Down Here  -->

## SeaGL etc
- Salt: 1. How are you all getting there (transportation)? Do you have room for 1 more?
- NEXT STEP (Salt): Connect Aaron and Daniel (friend)
- Salt: It would be nice to use SeaGL to get some social traffic (biggyback on hashtags, etc). I don't know how that mixes with changing priorities on gitlab.
- wolftune: How much to do big publicity stuff given state of the stie?
- Salt: I think the in-person interaction at the conference are worth having people show up who don't get initial ideal experience.
- Salt: We should have a good Initial experience when we do the announce-list though.
- wolftune: I was thinking it would still be good to have a blog post up this week. Deciding between "Hey, we haven't blogged in a year, here's a summary" and "Hey, we have a forum up now".
- wolftune: I'll do one this week, one later. Leaning towards forum. Thoughts? (+1 forum)
- wolftune: Announce the forum on discuss list? (yes)
- wolftune: Other thing that would be nice is having "roles we're looking for"..
- Salt: That would be nice, but don't push yourself for it.
- NEXT STEP (wolftune): Forum blog post
- NEXT STEP (wolftune): Social media outreach, mention we'll be at SeaGL
- NEXT STEP (wolftune): Coordinate about transportation, make sure to bring snowdrift stickers, etc
- NEXT STEP (wolftune): Adjust priorities of tasks on gitlab

## Design work-flow (and issue grooming)
- wolftune: This morning mray posted some bugs on gitlab.
- smichel17: I moved them from design -> code repo
- msiep: Bug (team link goes nowhere) is reported -> if there's a clear fix (place to link), fix it. If not, ask design.
- wolftune: Code repo or design repo?
- msiep: Code repo for bugs.
- wolftune: How is it that the link came to be there? It must have been on some mockup but not specified where it goes.
- smichel17: Let's try to track it down. Ask Henri?
- wolftune: Some pages aren't updated design. My impression is that this is because msiep has been busy. What if someone (eg, Robert) looks at a current page and it's not good.
- msiep: I suppose just open an issue.
- wolftune: You could also use the forum.
- msiep: I think the forum is appropriate when it's less clear if something needs to be done or what needs to be done.
- msiep: If there's no discussion needed, just a todo, no need to clutter the forum with task management.
- wolftune: Concrete example: our /about page is outdated. Issue or forum discusion?
- msiep: If it's clear that a new about page is needed, just create an issue.
- wolftune: msiep, what do you want <https://git.snowdrift.coop/sd/design/issues/70> to be in order to be useful to you? Assign it to you?
- msiep: Yes, and title should be more realistic. If it obviously needs an update, title can be that, not "Review the page"
- wolftune: I don't know if I need to say this, but the content there isn't decided on, it just happens to be.
- msiep: Deciding on content is interaction design, writing it isn't.
- NEXT STEP (msiep): Look through gitlab issues, do some grooming/organizing if you want.

## Code volunteers engagement
- wolftune: Some of these issues are so simple I can do them myself even (eg, fix the team link)
- wolftune: How do we make sure that all of the people who *could* fix the bug will get notifications that there is a bug to be fixed? Assignments vs @mentions vs other pinging vs knowing that people "watch" appropriate areas
- wolftune: Right now I'm not confident that this will happen (at least without doing it manually, eg, I've poked fr33domlover in irc about bugs)
- smichel17: I think some of this comes from our historical many places we
- wolftune: Make sure people are watching code repo and development part of the forum.
- smichel17: And probably site feedback part of the forum too
- wolftune: It'd be nice to have a list of people that we know are the people watching this. lists of volunteers, managing the extended non-team volunteer community; maybe forum group?
- smichel17: I think that's unlikely to happen.
- wolftune: I suppose civiCRM is really what I want; I'll just have to wait for it.
- NEXT STEP: Check in with team members and make sure they're watching the right places

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->