<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2018/ -->

# Meeting — October 23, 2018

Attendees: msiep, smichel17, wolftune

<!-- Check-in round -->

## Carry-overs from last meeting

```
## tracking ops tasks for just server maintenance
- NEXT STEP (Bryan): Review/capture ops priorities [chreekat, in his own list]

## Bryan/Stephen meeting — CI/CD improvements
- NEXT STEP (Bryan): write blog post about this process, document <https://git.snowdrift.coop/sd/snowdrift/issues/111>

## Governance overhaul / Volunteer recruitment
- NEXT STEP (wolftune): posts on discourse about things he has personal notes on governance roles.
- NEXT STEP (Bryan): Decide whether we want to open up option of moving off of haskell for our web framework.
- NEXT STEP (Aaron): Include this "call for frontend/backend split help" in discourse announce [added to draft: https://community.snowdrift.coop/t/please-join-us-on-the-snowdrift-community-forum/567]
- NEXT STEP (wolftune): co-working to create a more complete list of roles/titles we want (capture wolftune's notes in better form)

## Project outreach, Kodi etc
- NEXT STEP (Salt): Set up an hour to co-work with Michael
- NEXT STEP (Michael): work on feedback form <https://git.snowdrift.coop/sd/outreach/issues/41>

## CFPs
- NEXT STEP (Salt): Prepare CFP stuff, get feedback, check in at future meetings

## Private team contact info
- NEXT STEP (Salt): Spend an hour figuring out the right way to do this
- NEXT STEP (wolftune/smichel17): Remind Salt about this^ over the weekend

## Terms for *projects*; Code of Conduct docs (CoC, Values, Conduct-Enforcement, etc), relation to ToS
- NEXT STEP (): Announce + blog post https://blog.snowdrift.coop/ghost/#/editor/5bc65abb469e4600012c743d/

## Design progress plans
- NEXT STEP (msiep): Look through gitlab issues, do some grooming/organizing if you want.
```

## Metrics

Discourse (over past week):

- Signups: 0
- New Topics: 7
- Posts: 50

Sociocracy:

- Page 108

<!-- New Agenda Down Here  -->

## SeaGL
- wolftune: seagl is coming up in just over 2 weeks, at least Athan and I are planning to go
- wolftune: A good milestone…

## Michael and feedback stuff <https://git.snowdrift.coop/sd/outreach/issues/41>
- smichel: Michael should edit the issue for his own needs (really pushing for people to own their own issues)

## Core places for tracking work: GitLab, Discourse (bookmarks, drafts particularly), blog drafts, meeting carry-overs
- wolftune: I'm comfortable looking through gitlab, seeing things that are assigned to me, etc
- wolftune: I'm somewhat comfortable with discourse, just looking at the status of things (solved/closed, etc). Also bookmarks and drafts
- wolftune: There's also ghost drafts, and anything else??
- wolftune: I want a single reference place to keep track of all these locations
- smichel17: We have that! Or at least, this page is supposed to be it: https://wiki.snowdrift.coop/resources
- wolftune: Does it make sense to post in discourse about whether we should find a way to mark issues as settled, aside from the solved plugin
- msiep, smiche17: Just use the solved plugin
- wolftune: what about editing titles (eg, [SOLVED]), using tags? I'm just worried about unsolved stuff getting lost
- smichel: 
- NEXT STEP (wolftune): Edit <https://wiki.snowdrift.coop/resources> to make sure it's up to date

## Auth design <https://community.snowdrift.coop/t/confusing-non-standard-pass-reset-ux/1062/>
- wolftune: We need to do the full design process for auth
- wolftune: Bryan said he has no bias for the status quo, it's just there because it works
- msiep: What's the priority?
- wolftune: Reasonable, because the current one has issues, but doesn't need to be perfect; good enough that we don't lose people over this.
- wolftune: One big example: People do not expect to add 
- msiep: Also, there's no place to change your pw when logged in
- wolftune: There is an issue for that already.
- msiep: On the site, I tried to reset my password but I never got the email…
- wolftune: That is a major issue, high priority.
- smichel17: Michael, as our main UX designer, I think you (not Aaron) should make the call on priority; Is auth UX more important than an updated design on other pages?
- NEXT STEP (msiep): Post about your problem recieving password reset emails (draft: <https://community.snowdrift.coop/t/password-reset-email-not-received/1070>)

## Support UX
- figure out when/how someone with a support problem should deal with it
   - Especially when we might want to post private info
- where do we document ^that information?
- msiep: One place would be the contact page of the main site
- wolftune: Maybe also in the feedback-support category?
- msiep: How it should optimally be depends on snowdrift's funding status -> how much individualized support we can afford to give
- wolftune: The sort of person who's proactive enough to bother reaching out to us at this point is a potential volunteer; I think it's worth supporting them when possible.
- msiep: Yes, but if we have something like an alias, support@snowdrift.coop, we need to figure out who that goes to and make sure they won't 
- NEXT STEP (msiep) Post a summary and invitation for discussion of the concerns/questions around UX for support we provide as we discussed in the meeting (Draft: <https://community.snowdrift.coop/t/individualized-support/1069>)

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->