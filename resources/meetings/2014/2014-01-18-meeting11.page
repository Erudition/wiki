# Meeting 11

*January 18 2014, 9AM Pacific*  
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and live-recording of minutes on [etherpad](https://snowdrift.etherpad.mozilla.org/1)*

## Background readings / updates

Exploring the updated site, especially the discussion comments, tickets

### Assigned tasks from before

* Aaron: Knight prototype application plans (due by end of January), messaging, illustrations, coding. More coordination with Jim Hazard (maybe)? Other recruiting…
* Joe, David, Aaron: prepare for fake money testing, project sign-up, simple newsfeed/blog, simple updates to IRC bot, look into @snowdrift.coop e-mail functioning, permissions, moderation
* James: Graphic design consultation with sister, look over volunteer opportunities and come up with updated text
* Kim: Move notes for Standing Rules to wiki and work on improving them
* Mike: Review tickets, see what can do, any possible WebRTC updates still

## Agenda

* Check in with assigned tasks from previous meeting, agenda review
* Technical update

### Main item: discuss priorities in roll-out, when to go more public

* Site moderation getting adequate for more public engagement
    * How much is adequate? Promotion takes time and effort itself, so when to work on that versus just more work on the site?
* Tickets, tagging, other organizational parts of site
    * For engaging with our own team and with volunteers… how much is right balance here?
* Fake money testing
    * It's happening. What tests should we prioritize? Get more people involved and survey them for feedback? What stats to show? What next?
* Outstanding needs in prep for initial Board
    * Worth talking about formal Board yet? If so, formulate more specific plan.
    * Recruiting more need legal and financial advisors, potentially as steering committee members
    * Following up with Deb Olsen
* Recruiting representatives from potential Snowdrift projects
    * We don't yet have a technical system to add new projects. Get that done first?
    * Any other thoughts?

## Next steps assigned

* Aaron: write Knight fellowship app, due end of January, continue work on site, recruiting, contact Deb, contact Task Coach devs, contact Nina
* Mike: Come up with list of potential projects.
* Kim: Standing Rules, come up with at least one potential project, review Aaron's application if needed.
* Joe: Continue site development.
* David: Keep whittling away at tickets, be available in IRC and get new developers up to speed.

## Summary / minutes

* Progress made on tagging, fake-money testing, tickets moved to website, nav bar operational, blog near-operational.
* Progress NOT made on application process, standing rules, newsfeed.
* Please check in on the site ~ once a week to look at the progress, tag tickets, etc.
* Paradox of what to do next: recruitment vs. improving the site?
    * Anyone can register and make edits/post comments that will be moderated, do not yet have a mechanism for moderators to mark users as established or a list of unmoderated comments.
    * Good time to reach out to backend developers, as they can help with building.
    * As long as we have some way to remove inappropriate comments (particularly spam) approving unmoderated users is relatively low-priority.
    * Need way to show pending-approval comments before recruitment though
* Better planning for tickets? Need that to recruit more developers?
    * Tagging may happen organically after recruitment.
    * Tagging does encourage fixing.
    * Add back the wiki page listing to /t
* Fake money testing allows adding funds to account, daily payments going through, statistics display under construction.
   * Red banner for you've given x to y projects, click for details immediately on login.
   * Notification of significant share value changes
   * Notifications of messages
   * Transactions should be headlined / bannered / most prominent
* Informal committee vs. formal board?
    * Need formal board as soon as we start taking money, but do we want to do that sooner?
    * Recruitment / fundraising, need for real board?
    * Need to recruit governance people as well as developers. Put off formalization until we get the right people lined up?
    * Need people with accounting / finance experience (not specifically Board, but staff)
    * Find people / person with treasurer experience
* Where to recruit? IRC? Personal connections?
    * How much vetting do we need?
    * Focus on Haskell developers, recruiting initial slate of projects to make a good first impression.
    * Want to make sure people know we aren't just software, need to get some projects with staying power / established cred.
    * OpenBSD? LibreOffice as ideal software project?
    * Do we need an application process yet? Not high priority if we're doing curated projects.
    * Should at least list the forms for reporting honor status, even if we fill them out for the projects at first.
    * Want to prioritize projects that align very closely with our honor ideals.
    * Work with projects to determine what they want in terms of
    * Aaron will get other Task Coach folks involved to start really developing the listing
    * update Nina and see if she's up for testing things and how to list, questions concerns etc.
    * Neil Stevenson — Cyberpunk author ?
    * Corey Doctorow ? MIKE: maybe not people who are already tied to traditional publishing, given legal issues etc. AARON: could reach out anyway MIKE: really gonna be too much hassle, we should focus on Free Culture all the way where possible, we don't have as much pull right now to deal with people 
    * Reaching out to Deb: ask her for connections about co-op accountants
    * Priority of tagging system?
