<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/ -->
<!-- Meeting location: Mumble server: snowdrift.sylphs.net port 8008; backup option for video and screenshare: https://meet.jit.si/snowdrift -->
<!-- Group chat usage on bottom right of this page, please update username and choose color in the top right of this page -->
<!-- Bookmarklet to make the chat bar wider. Create a new bookmark with the below (select the whole line and drag to your bookmarks bar). You can adjust the width by changing "280" to whatever you want, in pixels.
javascript:(function () { const width='280'; const box = document.querySelector('div#chatbox'); if (box) { box.style.cssText=box.style.cssText+' width: '+width+'px !important;'; } const pad = document.querySelector('iframe').contentWindow.document.querySelector('iframe').contentWindow.document.querySelector('body#innerdocbody.innerdocbody'); if (pad) { pad.style.width=(document.body.clientWidth-width-50)+"px"; } })();
-->
 
# Meeting — March 16, 2020
 
Attendees from last week: msiep, wolftune, smichel, Salt, alignwaivers, mray
Attendees (current): msiep, smichel, wolftune, iko, Salt
 
<!-- Check-in round -->
 
<!-- Assign note taker -->
 
## Metrics
 
Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 3 -> 1
- Posts: 8 -> 5
- DAU/MAU:  33% -> 31%

Snowdrift patrons: 118 <!-- to be reviewed occasionally from https://snowdrift.coop/p/snowdrift -->
 
## Reminders on best-practice meeting habits
- Review previous meeting notes especially when absent!
- Audio notifications for etherpad chat available on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/
 
### Use chat in etherpad (and add your name)

### Conversation queuing
- "o/" or "/" means that you have something to say and want to be put in the queue
- "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
-  ">" as an indicator of understanding someone and the point can be concluded, please move on
- three discussion mechanisms: hand symbols (above), call for a round (and vary the order), open discussion
 
### Notetaking
- "???" on the etherpad means the notetaker missed something and needs assistance capturing what was said
- aim for shorthand / summary / key points (not transcript)
 
## Review previous meeting feedback
- salt: think it went pretty well, a lot of coverage - good balance of unstructured vs structured (maybe a little too unstructured at times)
- smichel: thought balance was good, the point where I got cut off I was fine with (for it to be that unstructured)
- wolftune: good meeting, feel free to cut me off more, I'm happy to be cut off and have the chance to insist on saying more when necessary


## Last meeting next-steps review

### New metric: DAU/MAU? (smichel17)
- NEXT STEPS (alignwaivers) add to pad [done]

### Surfacing tensions (wolftune)
- NEXT STEP (wolftune): include instruction for a 1-minute silence, review agenda, surface tensions, round for confirming thoughts [done]

### Conference/Individual funding user stories (Salt)
- NEXT STEP (miep): capture gitlab issue for writing User Stories for SPECS-STORIES.md to go along with the project-page and site-status implementations (add link (update project pages and that the site is a work in progress should both be user stories) [done?]

### Libreplanet (Salt & smichel17)
- NEXT STEPS (salt): update our LP forum event post [done]

### OSI incubation (wolftune)
- NEXT STEPS (wolftune/board): share the updates etc. and tasks for everyone's reference

### CiviCRM volunteer form etc. (salt)
- NEXT STEP (salt): discuss this next time
- NEXT STEP (msiep): capture task to make basic wireframes for these landing pages


<!-- One minute silence, check with ourselves mentally and personal notes/tasks/emails to surface any tensions, add agenda if appropriate -->
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->


## Current Agenda

<!-- New Agenda Down Here  (Added 48 hours before the meeting or earlier)-->

### Libreplanet recap/reflection (smichel)
- smichel: I felt there would be more engagement in person, didn't end up finding anyone who wanted to volunteer to do frontend dev
- salt: yeah, harder to bring up but different, had volunteer form but forgot to use it, darn
- salt: did get some eyes, good energy, a couple people showed up at BoF and we got a request from some BoF people for a recap for those who couldn't make it, maybe we could write a blog post about that
- wolftune: Hallway track couldn't quite be the same virtually, didn't feel like there was a particular time to use the generic conference channel
- Salt: joeyh did a lightning talk about virtual hallway tracks, I haven't watched it yet\
- wolftune: were other people able to attend the online events, e.g. listened to talks?
- msiep: I didn't realise it was so soon, would have liked to participate more but might not have had time
- iko: listened to a few talks, including Salt & Micky's, lurked at BoF but didn't have anything to add. thought outreach team had questions covered well
- wolftune: I'm not as confident doing a libreplanet summary blog post, maybe co-author?
- smichel: I could co-author, not super sure what to put in it.
- Salt: If you get a list of questions, I can answer them, just can't spend the time to do the whole thing
- NEXT STEP (wolftune): prepare blog post summarizing how it went from our view and what got discussed about Snowdrift.coop

<!-- Late New Agenda Down Here (Added within 48 hours of the meeting)-->

### Keeping connected etc (wolftune)

- wolftune: I was thinking we've got a lot of experience being remote, but I wanted to open up the topic of snowdrift remote work fits into the current scheme of things
- ROUND:
    - wolftune: I'm trying to anticipate what my schedule will look like, will try to have music lessons at particular times, figuring out how that fits in with remote, won't be community to portland on sundays, gives me a little more flexibility. Will still have family distractions. Want to figure out how to make the most of the time pushing snowdrift forward.
    - smichel: given I'm already working fully remote, my availability hasn't changed. Previous April deadline for work presentation event was technically cancelled, but we're still trying to use it anyway. I'm around on weekends in particular, but not a glut of availability
    - Salt: got class stuff to do for next quarter, I need to get to the point to be able to work on snowdrift more again
    - iko: my availability hasn't changed either, did update meeting availability a little back, it's still up to date
    - msiep: it doesn't affect me that much, since I am completely remote (besides kids being home, school closures).
- wolftune: Felt like talking about coronavirus is a thing we'd be doing if we were in an office, so wanted to do it a little here too. I'm up for coworking.

### OSI incubation and Board (wolftune)

- wolftune: I haven't made progress on this but want to. A little uncomfortable with current status. I have some emails, but goals should be better captured by the end of the day

### Wiki link in footer? (wolftune)

- wolftune: this came up during the BoF, someone wanted to check out the wiki and couldn't find it easily. They asked why the link wasn't in the footer
- msiep: I think it would be good to put in the footer. Someone on irc mentioned gitlab as well. The only thing is that the more things in the footer, the harder it is to find other stuff.
- wolftune: yeah, I'm not sure about gitlab, it's an external site, there are other places to link to it as appropriate
- msiep: If were were going to include a wiki, I just need to decide where it should go.
- wolftune: you can try to add the code for it too, if you like
- msiep: Need to figure that out again, is that where I need to get the site building locally?
- Salt: Gitlab isn't internal to our domain name, but it's still one of the tools we use, and I think that's a common thing for footer navigation — "I'm familiar  know where I want to go, just trying to find the link to get there"
- Salt: I don't think it needs to be called gitlab, maybe source repository or something, but I suggest it has a place down there (foooter)
- msiep: could it be called "git"?
- Salt: Sure, want to answer the user story, "I'm a developer, I'd like to find your code (quickly)"
- msiep: I'll take a look for both
- NEXT STEPS (msiep): look into adding links to wiki and gitlab in the footer

### Todo tracking (smichel17)

- smichel17: Tension: There are a bunch of things that used to be tracked as carry-overs here, that are now in my personal list, but my personal list is not quite back to being functional yet
- smichel: I know there are a bunch of sd tasks sitting in my inbox, but unsorted. On one hand it's fine because when I do put in more time, sorting them will be the first thing I'd do, but if there are any that are time-sensitive, I wouldn't know 
- Salt: I have a similar phenomenon. I'm doing a bit of a bullet journal-type thing to move tasks over and sort them
- Salt: every time I get to a new month, or far along, I move tasks over to the next month (actively instead of passively)
- wolftune: project management is hard. I think we should err towards more discussion of this type of thing, e.g. checking in more with people "how are you doing with x task?"
- wolftune: Don't like the idea that anything todos for the project are not captured in a project-owned place. I would like next steps to be captured in some format
- smichel: Proposal: reminder to look at personal task list during 1min reflection section of meeting
- smichel: that might be enough to feel like I'm not losing track of tasks
- NEXT STEP (wolftune): Add reminder to look at personal task list to 1min reflection step [DONE]
- Salt: I think that was very valuable to bring up
- wolftune: if someone is checking personal notes & realizes there's something there, not captured in group project area, but not needing discussion…  wonder if there should be a method in the meeting, separate from agenda. Have a backlog of capture-now stuff to do prior to the meeting
- wolftune: We tried earlier, making sure everything gets captured right away at the end of the meeting. That didn't work so well.
- wolftune: What do we do with things that need no discussion, but aren't captured in a snowdrift project management location?
- Salt: Maybe just a standard meeting item? but make sure when you bring it up you're framing it as "this does not need discussion"
- Salt: Or that you have a tension around someone else not having done it.
- Salt: similar tension to when there's a next-step from last meeting, but the person who's supposed to do it isn't there. Someone new takes on a task to check in with that person about it.
- wolftune: Okay. Do need to make sure that other similar items, e.g. smichel's list of tasks gets captured somewhere?
- decision: we'll let things get surfaced next meeting since we've now got it explicit in the meeting

### Volunteer form (salt)

- Salt: we don't have a good url to update the volunteer profile, so for now people are asked to contact community@ email if they wanted to update
- Salt: I will continue to work on it on the side
- smichel: I can't see it at the moment, just a 404
- Salt: try it again https://contacts.snowdrift.coop/volunteer
- iko: do others have the DNS entry in their hosts file?
- iko: add this to your hosts file (/etc/hosts on linux): 140.211.9.53 contacts.snowdrift.coop
- Salt: yeah, that's a temporary measure until the instance is fully launched
- Salt, msiep: some of the fields aren't aligning left on my browser (different fields for Salt's phone and msiep's device)
- iko: can you let me know later which browsers you're using?
- Salt: eventually we'd try to a sub-theme for civicrm
- msiep: I don't think "other" interest checkbox makes sense as the first option
- Salt: the list of interests is currently randomised, I can disable that
- msiep: yeah, it's really used to avoid introducing bias in the response, but in this case I think there isn't a difference
- msiep: is there a reason the notes below the experience heading are bold? Can it be smaller and not bold?
- iko: most of the font weights follow the default theme. That specifically that was a design guide default, to make smaller font bolder, but sometimes it depends on context. Sure, I'll adjust that.
- msiep: is it possible to move the notes between the heading and the textarea instead of under the textarea?
- Salt: I'll look into it in a bit
- NEXT STEP (Salt): ping OSU, push this live [DONE]
- NEXT STEP (Salt, msiep): Post any bugs to gitlab issue

<!-- Open discussion? ~5min. if time -->


---
 
## meeting evaluation / feedback / suggestions round
- Salt: overall good
- smichel17: overall good meeting, felt unnecessary at end, but coworking good, would be nice to not have it creep into meeting
- smichel: I like the one-minute and tension-bringing-up at the beginning, so much that I almost feel like I want the meeting to be *entirely* stuff like that and leave the "topical" type stuff we've been doing for forums (or for dedicated tactical meetings with appropriate people)
- smichel: Feel like goodbye round could be less moderated, more natural/freeform
- wolftune: lots of thanks, all have good tools but don't need to use pedantically, good balance
- iko: pretty good, thanks Salt for facilitating and smichel for taking notes :)
- misep: good meeting

<!-- Goodbye round -->
<!-- Capture NEXT STEPS -->
<!-- Clean up meeting notes, then add to wiki -->
<!-- Prepare this pad for next meeting: (A) replace previous meeting eval notes with new (B) Clear discussion notes, moving NEXT STEPs to "review last meeting's action steps" (C) Update next meetings date, clear attendee list  (D) Update old metrics, update date, leave new blank -->