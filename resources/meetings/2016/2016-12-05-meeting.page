
# Meeting — December 5, 2016

Attendees: chreekat, iko, mray, msiep, salt, wolftune

---

## Agenda

- Discourse SSO (salt)
- Team goals (chreekat)
- Finish intro (mray)
- Discourse misc tasks (salt)
- Deciding on announcement date (aaron)
- How to use Taiga (wolftune)
- Updating wiki (wolftune)


## Discourse SSO

- salt: there's been work on this. I pulled the trigger on that without general consensus in a sense, but I'd really like the public to be able to log into Discourse with the same
- jazzyeagle started work on it and fr33domlover also worked to make this a module
- jazzyeagle is currently testing it on a local discourse instance
- chreekat: this is why I wanted to start with "Team goals"
- this reboot was about writing good code, not diving into code. we should discuss what we want to achieve
- I feel like this is a big fundamental disagreement of opinion, so we should start talking about the things we want to do
- *(proceeding with "Team goals"  as the next item on the agenda)*


## Team goals

- chreekat: I sent out a ML message today
- salt: the one thing that jumped out at me are the numbers, e.g. $6000/month
- it sounds like someone needs to keep track of accounting
- wolftune: I would be involved for sure. in general, there's the question of what options we have?
- e.g. if we had to progress with fewer resources than we currently have, what does that look like?
- possibilities between ideal and failure
- chreekat: I'm going to find other work and be a snowdrift volunteer, to reduce snowdrift expenses
- the bar can then be set pretty low
- wolftune: also, how much work is there from here to arrive at a stable state?
- chreekat: other development tasks
    - design architecture framework (getting sass in and so forth)
    - "smoothing down the rough edges for upcoming announcements"
    - crowdmatch mechanism tweaks to meet short-term financial goals
    - actually writing the crowdmatch and process-payments utilities
        - Coordinating with Stripe for many-to-one transactions
- we should start sanding rough edges, skip the tweaks
- salt: you're saying skip #3? how about #1?
- chreekat: the current site wasn't re-designed, it uses existing css
- salt: I'm fine with skipping #3
- wolftune: I'm thinking of getting out an announcement on social media, ML, all the people we connected with, in two weeks
- we'll have time at the end of the month, to evaluate the donations and turn the knob to adjust amount for people want to donate more
- mray: to comment on chreekat's list, we should guarantee a certain level of quality
- we should be humble and not too loud unless we have something to be proud of
- including avoiding secondary channels to donate on the side and focusing on getting things better
- wolftune: I'm inclined to go for the long haul
- davidthomas is no longer contributing funds towards development
- he wants to do it with the actual crowdmatching, and how to give as much as he can via the mechanism
- mray: this is currently not set up in a way to donate as much as he wants
- msiep: there's also the question of when the pledges will get charged
- at the current rate, there will have to be thousands of patrons before that happens
- wolftune: that's specifically because we have only have 1 project at the moment
- msiep: then the knob that david is interested in may be critical. there will be people who pledge then forget about it for 3 years because nothing happens
- chreekat: if we cannot cater to that subset of patron, then what's the point?
- wolftune: as an additional comment, I read the video script to a friend for feedback, and first thing she asked is, "can we set the level that we're matching at?"
- chreekat: let's wrap it up with actual goals
- December is about engaging the "shut up and take my money" crowd
- I'm not sure what that means exactly. I don't know what the goal is and would like to set one
- wolftune: I think we should still make an announcement
- salt: I disagree that we should be making announcements any time before we're ready, for people who haven't seen the website for a long time (and be impressed)
- wolftune: what goals do we need to be able to get there?
- mray: for me, the goal would be to have msiep lead the way in making the UX good enough, get feedback from others on the experience
- salt: this goes back to chreekat's list
- chreekat: what if we set a goal to get half the number of registered users pledging?
- mray: what if they start using the service and get the impression that it sucks?
- chreekat: how would they know?
- mray: they suddenly leave the project/site
- chreekat: how about a goal of 50 patrons? we will know who they are
- I'm trying to visualise what our goal looks like. do people have other goals or possible visualisations?
- wolftune: I wasn't proposing what the goal should be, but a topic
- what snowdrift is, what pledging does, etc.
- salt: that's not a concrete goal
- wolftune: but we can see that people have a better understanding of the project
- chreekat: how do we observe that? we observe that by a steadily increasing patron count
- msiep: at what point do we feel comfortable approaching strangers with a usable site?
- initially, a good way would be to ask, "is this something you would tell your
  family and friends about?"
- mray, wolftune: agreed
- wolftune: qualitative vs. quantitative goals
- chreekat: we've more or less reached the "shut up and take my money" phase
-  we need to increase the number of patrons. the next group would be close friends and family
- salt: friends and family may not have any background, I think we could start on irc, where people are aware of the project
- msiep: before talking to friends and family, we want to talk to people who are already enthusiastic
- salt: proposed announcement circles: partners, personal contacts, irc, non-announce mailinglists, close friends and family, people who have signed up previously and announce list, social media
- wolftune: we can reach out to different circles
- chreekat: I don't like this talk of circles and defining them technically. wolftune and salt are the ones who know who are interested. irc is a mixed bag
- salt: I don't think this should be done in real-time, let's end the meeting for now
