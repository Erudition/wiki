# Meeting — December 12, 2016

Attendees: Iko, JazzyEagle, mray, Msiep, Salt, wolftune

## Checkin

- iko: done with Discourse colours, please report any strange colour bugs
- jazzyeagle: got everything together testing Discourse SSO
- spoke with chreekat about tests, got 3 test cases so far
- mray: busy with family, not much snowdrift-related stuff
- msiep: started on feedback for site login
- salt: crazy weekend, grad school application due next week
- also, Happy Birthday to me!
- wolftune: still helping parents move and complications with moving transport

---

## Agenda

- Gathering feedback? (mray)
- When/if to directly edit site via git? (MSiep)
- Process for public bug reports/feedback (iko)
- Intro script (mray, MSiep)
- Discourse (salt)
- CiviCRM (wolftune)
- Site content, wiki content (wolftune)
- Documentation for volunteers etc (wolftune)
- chreekat (salt)


## Gathering feedback?

- mray: what's our process and tools for getting feedback for the website?
- what's our plan, do we wait for standardised material?
- is there something we can do beforehand?
- salt: depends on the timeline, with msiep's help we can get something together
- msiep: I think there's some room for improving it ourselves first
- mray: when do we expect will be the time to say we're ready to get feedback?
- msiep: there are two slightly different perspectives, we welcome feedback and we also hope people like it and just become a patron


## When/if to directly edit site via git

- msiep: in some cases, it may be more efficient to make changes directly and submit via git
- e.g. changing login fields to something simpler
- I don't want to overdo by having discussions for every little detail, but don't want to underdo either
- wolftune: refer to Holacracy. as UX lead you have accountability to make changes
- check with people when you want someone's opinion, we can always discuss the changes on gitlab and revert if need be
- msiep: I'm unclear who has accountabilities or holding positions and how/when feedback occurs, e.g. mray is co-UX lead, etc.
- if I made a change and sent a ML message explaining why, we could have discussion (i.e. what should the process be?)
- salt outlines briefly how to use gitlab
- wolftune: you can also list what you did, as well as comments on specific lines of code to explain changes
- you can also propose changes as an option to merging directly (MRs)
- mray: we can also use taiga. how do we decide when to use where?
- wolftune: good question, we should deprecate the ML and move to Discourse asap
- mray: so we don't really use gitlab?
- wolftune: I'm not sure where that fits in, but pointing out the migration to Discourse
- salt: taiga is more an internal tool, but the discussion shouldn't be on gitlab. all public discussion should be on discourse eventually
- mray: for example, if msiep makes changes to html, he creates a MR and would in my view qualifies an extra post in discourse for review?
- however, there are people who aren't developers yet, they might give feedback
- wolftune: we want to encourage to join in gitlab and give feedback. there's an overlap, it's a judgement call in each case.
- gritty details in gitlab, changes that generate broader discussion elsewhere
- mray: should we have a guideline on what goes where?
- salt: if the discussion goes beyond several posts, we can have a few gitlab moderators, who watch the conversation and direct people to discuss it in discourse
- wolftune: the main thing we use taiga for is sorting priority and tasks to help people stay focused
- mray: so we do not have discussions in taiga?
- wolftune: we don't discuss the issue itself in taiga, just the priorities
- salt: I don't think there will be a lot of discussion in taiga
- mray: I can see this getting mixed up quickly
- salt: I'd like to table this discussion for later

Next steps: mray can summarise the discussion (in Taiga as a US, since this is a partner discussion), then later link to Discourse (when it's up)


## Process for public bug reports/feedback

- iko: related to previous agenda items, there's some confusion regarding how the public files bug reports/provide feedback in the interval before Discourse is ready
- people can open a taiga IS but cannot comment
- salt: let's move on and discuss this in a taiga iS

Next steps: iko should ask via Taiga IS


## Intro script

- salt: until we have a finalised script, it would be good to have a real-time discussion to finish it

Next steps: organise another meeting to refine script


## Discourse

- salt: setup is almost done, the colours are snowdrift-like
- SSO is the technical blocker, everything else is text updates
- jazzyeagle: the SSO stuff is complete but it's waiting for tests to be
  written
- I can send you a MR and you can install that on the Discourse side now, or you can wait until tests are done
- salt: I'm not going to get to it until late this thursday, so let's wait for the tests
- thank you so much for getting SSO done
- jazzyeagle: thank fr33 for that, I only tested it
- salt: then the energy put into it
- jazzyeagle: np
- wolftune: is there an option to set what the flagging option looks like, for people who still need to accept the honour code?
- salt: yes. can you add that to taiga issues so they don't get lost?
- Discourse US: <https://tree.taiga.io/project/snowdrift/us/146>
- also, iko, can you look at that US and copy off the old, unrelated discussions between chreekat and smichel17 elsewhere (wiki)?


## CiviCRM

- wolftune: I was already marking some items there. are there things others (e.g. Athan) can help with or are we waiting for things still?
- salt: mostly done, just contributions are not yet moved. maybe someone can check if anything's missing?
- woltune: relationships were missing
- what about interaction with other systems? how do we can keep track in civicrm of people submitting git requests, etc.?
- salt: going forward, there's already a slot for external ID. I have to confirm with chreekat what number goes there, but that's the SSO ID


## Site content, wiki content

- wolftune: I want to go through the wiki content and making sure things are updated
- I'm not sure what to discuss about this, do people have comments overall?
- mray: is it about my ML message on deleting the /design/site-design page? how do I move it to /archives?
- iko: it can be done via gitit (create new page in /archives/design, copy and paste contents, delete the old page) and gitlab (move the file)
- wolftune: I'm not entirely sure about this but if you just move the file via git, it should reference the move and preserve the history of changes for the page
- salt: I would say the wiki isn't really a priority
- iko: the wiki content is a priority where the main site links to some of the wiki pages
- (clarification: overall though, I agree, e.g. for technical setup)
- wolftune, maybe you could check with smichel17 if/when you see him?
- he had some feedback a while back about people coming to the wiki and having a good reading order of intro pages, which we already did address
- (there is already a US tracking this not yet closed, waiting on review?) <https://tree.taiga.io/project/snowdrift/us/147>
- wolftune: yeah, and I agree we should be cautious about making sure the pages are updated when the site links to the wiki
- would someone like to speak with smichel17?

Next steps: salt will speak with smichel17 the next time he logs on


## Documentation for volunteers etc.

- wolftune: a lot of stuff mray mention earlier, how to get involved, etc. that should be clearly documented.
- making sure the new code is well-documented
- we don't really have time to discuss here, but noting that it should be done sometimes
- I'm figuring out the scope, who will do what to get this to the place it needs to be
- do we even have a US that says "collect/mark/tag things volunteers can get involved in, guidelines, etc."?
- salt: there's guidelines for volunteer intake
    - <https://tree.taiga.io/project/snowdrift/us/115>
    - <https://tree.taiga.io/project/snowdrift/us/187>
- wolftune: that's good, though it doesn't sound like it fully covers what I'm referring to

Next steps: wolftune will open a new US for volunteer documentation


## chreekat

- salt: has anyone spoken with him in real-time since his email?
- *(responses were "no")*
- I'm bringing it up because it means reduced development time on snowdrift
- wolftune: we have haskell people interested in development, and before it was more an obstacle for chreekat to have a lot of people making commits while he was cleaning things
- now that the codebase is clean, we're ready to reach out to other haskellers to help with development
- salt: I guess a blocker on that is getting discourse ready
- wolftune: another thing is waiting for chreekat to update developer documentation
- salt: so in terms of checking with chreekat, there's developer documentation and keeping an eye on MRs and architectural stuff?
- wolftune: yes
- salt, wolftune: the next step would be to have a US about updated developer
  documentation to track progress

