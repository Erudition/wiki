# Meeting — November 27, 2017

Attendees: iko, mray, MSiep, Salt, wolftune

<!-- Agenda -->

# Specs/stories

- wolftune: I feel good about the current process, but would like to do a round-robin thing. I'll describe the situation. and people ask if there are questions.
- chreekat requested people use a specific US format ("As a ___, I want ___ so that ___"). I think it's a good format.
- steps:
    1. current spec-stories in the repo reflects what matters in the repo
    2. make issues to add new US or update existing US, when the work to implement is also in the repo
- specifics not covered yet:
    - who starts issue where?
    - when is US good enough? what happens next?
    - when consensus reached on a US, when does it go from issue to … ?
- minor disagreement during wolftune's and chreekat's discussion: separate issue for deciding the US and implementing US (wolftune) 
and keeping them all in one issue (chreekat), but wolftune is also okay with decision to use one issue
- Q. (mray) what's the goal of having specs and the stories?
- A. (wolftune) as a project team member, I want to be able to have a clear, point-of-truth reference during discussion
    - reference to talk about what is currently there or planned
    - current specs-stories is after-the-fact doc (what would be drafted if there had been no existing site)
- Q. (mray) I'm confused about how to use it in the current situation, given it is only for creating new items?
- A. (wolftune) you don't use the spec-stories doc directly, we make new issues and only reference if we change anything
- Salt: chreekat is not here, let's discuss after the meeting
- NEXT STEPS: bring up at next meeting

# Terminology

- wolftune: <https://git.snowdrift.coop/sd/wiki/issues> numbers 3, 4, 5, 6
- getting feedback on what terminology to use when discussing things, to improve anything we need clarified
- Salt and mray will find some time later to give feedback
- NEXT STEPS: work asynchronously on the issues and bring up at meetings as needed

# Mail Server

- wolftune: I contacted Kolab, they said they will forward to management and get back to us with options, waiting to hear back
- also thought about openmailbox.org
- iko: <https://www.ghacks.net/2017/08/07/openmailbox-alternatives-after-owner-change/> doesn't look good?
- NEXT STEPS: wait for Kolab response

# Hosted Discourse?

- Salt: have we thought about reaching out to Discourse to see if hosting is an option?
- wolftune: idea leads back to other sysadmin apps, maybe send message to ML and see if anyone has contacts
- chreekat said he was going to try getting Discourse up during the meeting
- so, at least not priority to reach out to them
- mray: a week ETA for shutting down ML?
- Salt: not shutdown, but migrate
- NEXT STEPS: chreekat continues migration process


# Carry-overs

```
Carry over: specs-stories
NEXT STEPS: wait for other stakeholders to be present to discuss further

Carry over: when to announce working pledges?
- Salt: please hold off on announcement. has an issue been made?
- wolftune: not yet
NEXT STEPS: create/update issue to make sure people know whose opinion we’re waiting on

Carry over: iterating/deploying
NEXT STEPS: deferred (until chreekat/devs are present)

Carry over: librejs payment processing
Assigned: wolftune: email rachelfish info from pj and crowdsupply etc.

Carry over: tools for managing outreach
Assigned: Salt: Salt decides this stuff, pings wolftune

Carry over: initial board
NEXT STEPS:
- plan board meeting, set available time, contact everyone
- gather list of resources and advisors to contact
- reach out to advisors with reasonable set of questions

Carry over: update on the video
Assigned: wolftune: final audio recording done

Carry over: legal email, discussion, etc. from <https://wiki.snowdrift.coop/resources/meetings/2017/2017-08-11-meeting#next-steps-3>
Assigned: wolftune

Carry over: Holacracy thoughts: smichel: make report about what we’ve got so far, what seems to be working, and what we would need to do to go further
Assigned: smichel

Carry over: prioritizing iterative work process / dev
Assigned smichel will document this process better: find tasks: Kanban → US → internal tasks for the US

Carry over: code policies, specs for code modules
Assigned: fr33 will talk to chreekat, get him to document his criteria/policy for code merging and onboard other maintainers; also mechanism specs written out

Carry over: Discourse launch
Assigned: Salt: back up existing Discourse to allow safe testing prior to full launch/use

Carry over: add notes from wolftune’s feedback to Salt about his SeaGL presentation to appropriate wiki page with presentation resources/ideas
Assigned: wolftune

Carry over: Salt describes a process for how we’ll use CiviCRM to enter and tag prospective first projects
Assigned: Salt
```