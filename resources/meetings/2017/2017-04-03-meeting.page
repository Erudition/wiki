# Meeting — April 3, 2017

Attendees: Salt, mray, JazzyEagle, iko, wolftune, MSiep

---

## Agenda

### Materials for LFNW and static storage (iko)

- iko: two related questions for discussion. Salt gave a Lightning Talk last
  month on short notice and ended up using older FOSDEM slides
- it would be nice to have a static storage, or some location where people know
  to find conference materials and other presentation assets
- what's the possibility of setting up something like that, e.g. any space on
  mx1?
- do you need any materials from Design for LFNW? (anything I can help with,
  leave the materials for you at the storage location, etc.)
- Salt: I'm a little torn, as I could see this being in GitLab and also posted
  in Discourse
- do you remember the handouts you had before? the one with the democracy and
  other points, 4 to a sheet?
- we still have some of those, not sure if we want more posters
- I don't have confirmation yet if have a table (we didn't get any actual
  snowdrift talks)
- wolftune: we have the snowdrift dilemma poster and the banners
- Salt: I would like to get Discourse going, I felt a little bad at LibrePlanet
  we didn't have that available yet
- mray: when does LF start?
- Salt: I'll be up there May 3rd, so about a month from now
- as far as static file storage, we can have the storage on GitLab (or maybe
  Seafile, not on one of the servers) and add a sticky in Discourse linking to
  the latest versions
- mray: I think Seafile is working well, are we switching to git and
  Sparkleshare? (it's easier to manage latest versions in Seafile than git …)
- Salt: we're not talking about switching applications/storage for design
  files, so Seafile or GitLab is fine. if most of our files are stored on
  GitLab then I don't think we need a separate static storage solution
- **Next step: iko adds a Taiga US: create Discourse welcome category to hold
  threads with important information to each category**


### Matrix?! (mray)

- mray: I noticed a number of you are using it, how far is this in our official
  repertoire?
- Salt: it's an awesome bridging protocol, it's kind of new, but we're still on
  irc
- mray: did we set up our own server and are we replacing irc in the long run?
- Salt: I think we're running on the matrix.org server and bridging to IRC, but
  I don't think there are any plans to discontinue IRC bridging
- riot.im is neat as it puts GPG on top of messaging


### State of funding; funding mechanism question (JazzyEagle)

- JazzyEagle: how is the funding mechanism working so far and what are our next
  steps? are we getting money in?
- wolftune: aiui, the payout part isn't complete. people pledge and it adds up
  the count, but the payout part isn't active
- chreekat had the intention to implement it in a few weeks before he got the
  full-time job (outside of snowdrift)
- JazzyEagle: the mechanism isn't fully fleshed, the info needed for the
  dashboard isn't there yet
- Mitchell is starting to look into it, cgag said he was going to look into it,
  but do we have a plan going forward for finishing the mechanism? anyone else
  working on the mechanism?
- wolftune: besides chreekat, not at this time, I think this is related to
  CiviCRM. there are some people in the haskell community and get people
  working on it
- Salt: CiviCRM was one of the prerequisites to the launch announcement
- JazzyEagle: I just wanted to make sure we're not putting the cart before the
  horse, given the LFNW/conference announcements
- we've been working on this 3 years now, I fear people will turn away if we
  don't get something up soon
- Salt: I feel that right now people are wondering if we're static, and showing
  people we're still working on it is the important response. the pledging part
  is functional
- JazzyEagle: it seems to me the mechanism should be the important focus, yet
  we don't have anyone working on it
- wolftune: JazzyEagle, I think your concern is spot on. aside from waiting for
  chreekat to return to working on it and not waiting until we have everything
  in place and perfect to do that outreach
- I think I'm going to focus on the video script as the first thing, but feel
  free to ping haskellers on IRC
- JazzyEagle: I haven't used CiviCRM and don't have access to the lists, so
  don't know how much I would be able to help with the outreach
- Salt: I'm at a bar at the moment having trouble accessing their wifi
  (connecting to Mumble from mobile phone) and smichel17 is away today, so
  we've postponed the CiviCRM tour to next week, but if he's not around next
  week I can do a walkthrough with you anyway
- wolftune: we can let people like singpolyma on IRC know we're looking for dev
  input/contributions
- JazzyEagle: we can send out a dev ML announcement
- wolftune: that's a good idea, thanks for bringing up the matter
- mray: we should do something about our activity visibility in general, we
  don't even have a blog
- wolftune, Salt: yeah, we're aware of the issue
- iko: is it a question of making the decision on what to use, or no bandwidth
  to implement it?
- Salt: bandwidth, been busy between CiviCRM, conferences, etc.
- I think we (Outreach) were looking to add a Ghost blog, dockerise and connect
  it to nginx
- iko: okay, I could probably help with the docker script
- wolftune: there's also the GNU Social and other social media accounts, we can
  announce conference presences
- Salt: I currently don't have auth access to them yet, I'll get to it later
- mray: what do people think of posting meeting notes as activity?
- Salt: there are a few downsides when we do governance, but I think it's a
  good thing overall
- iko: side comment, we may not always have meeting notes. I had a brief
  conversation with smichel17 about it, aiui his perspective is the outcome of
  meetings will be reflected in Taiga timeline and US
- Salt: personally, I disagree. even though I might not read all the notes, I
  like having them at hand later if needed
- mray: yeah, we don't have to show everything, but more information would help
  with visibility
- **Next steps:**
    - iko will look into dockerising ghost
    - Salt will arrange with wolftune to get auth access to social media
      accounts


### Discourse (Salt)

- JazzyEagle: a comment about Discourse, Mitchell asked about some
  implementation detail for the Discourse SSO and it's been a while since I've
  touched the SSO commit, so that's the back and forth you've been seeing on
  IRC


### Governance and elections (Salt)

- Salt: I would like to have elections with 1 week advance notice, any
  thoughts?
- *no objections from those present at the meeting*
- Salt: iko, any problem with the current situation and continuing to take
  notes
- iko: no problem, maybe check with smichel17 too (he was formally elected for
  the role)? also, thanks for facilitating :)
- mray: I second that, thank you Salt
- **Next step: elections to be announced in 3 weeks**


### snowdrift.coop landing page alert (mray)

- mray: a while back we discussed removing the notification off the front page,
  but it's still there
- iko: yeah, I think it's just people working on a different branches (i.e.
  it's already updated on the alpha-ui-design branch) and no one working on
  master
- Salt: so someone will have to make the changes on master and get it deployed?
- iko: right
- mray: maybe we can ask wolftune about it? he's the most familiar with master
  state
- Salt: since wolftune has stepped away at the moment, we can ask him about
  this on IRC and conclude the meeting for now
- wolftune (post-meeting): I'll make that a top-priority as an immediate thing
  to do right now and ping chreekat to do a deploy
- **Next step: wolftune will edit the landing page and make arrangements for a
  deploy**
