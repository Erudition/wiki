# Meeting 4

**Sat May 18, 12PM Eastern**  
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and [etherpad](http://etherpad.wmflabs.org/pad/p/Snowdrift20130309)*

## Agenda

* review of minutes from [meeting3](meeting3)
* report from Aaron about the status of things and progress since last meeting
    * first more-public roll-out
        * [press](press), etc. [Postmag.org](http://postmag.org/could-snowdrift-be-the-future-of-crowd-funding-creativity/), [H-online possibility](http://www.h-online.com/open/features/Free-s-a-crowd-funding-1847399.html), Karl tweet, [Reddit](http://www.reddit.com/r/haskell/comments/1dx7ui/new_funding_platform_for_free_software_art/)
    * discussions with RMS, FLOW
    * engagement with FCF list and more sign-ups, interest
    * legal status: Brandon stepping down, unsure now about 501c3, need to seek new counsel
* report from David about technical status
* possible hiring of Kim Hoff as well as welcoming her to committee
    * See her [application](/application/12)
* discussion about committee, dedication, aims
    * Nina stepping down
* any other new business / discussion
* adjournment

## Minutes:

Attendees: Aaron, James, Greg, David T.

Meeting topics:

* review meeting 3
    * role of committee as advisory, casual consensus
    * code for America?
    * James joined last meeting, has been active

* Aaron: status report
    * make site more public, upfront donations, ripple
    * homepage status, much better, but maybe move some detail to separate page?
    * as we get into publicity / press stuff (see agenda above),
        * maybe wait until we have a few more details figured out to make access for wiki participation easier, etc.
        * maybe we need clear terms of service when we start getting more contributions
    * Aaron's communication w/ RMS:
        * RMS requests removing "open" from title page due to attached issues
        * We're sticking with Free/Libre/Open (FLOW) anyway, considering the complex balance of all these issues, see [free-libre-open](free-libre-open)

* Legal issues
    * Articles of Incorporation were not accepted due to ambiguity about coop structure
    * now further concerns about whether we may not fit the qualifications of 501(c)(3)
        * Would 501C3 be problematic for future development of Snowdrift?
        * IRS problem: site could be used by a business to benefit the business -> not appropriate for 501C3
    * Brandon, Snowdrift legal council, stepped down
    * Greg: Use other filing type (ex: LC3) which may be more flexible and maybe easier?
    * Aaron: other lawyers suggested 501(c)(4) -> still non-profit but maybe easier, less hassle, more likely
    * Need new legal council needed

* David's friend Kim Hoff: maybe helpful & maybe hire her
    * See [Kim's job application](https://snowdrift.coop/w/meeting4/comment/313)
    * how to set up compensation?
    * Greg: SPARK maybe helpful to answer questions about this
    * Should we also accept her onto committee? See [her committee application](/application/12)

* David's status
    * made wiki diff system, still needs GUI though
    * some other bug fixes
    * no other real progress since last meeting, need to find solid block of time for work
    * pledged to use next weekend to get things done

* Committee issues: 
    * Nina stepping down from committee but stay on as editor
    * David L. MIA, should send e-mail, ask if he's going to stay on
    * Mike L also MIA. Forgot about call, will be offline mostly for a couple of weeks. (Mike has otherwise attended and been available and responsive here and there)
    * Ways to promote committee membership? Perhaps more delegation, commitment
        * Greg: will send along ideas from other org about delegating / inspiring committees

* Next milestone:
    * "Project in project": making wiki and user permissions available to all
    * Running "fake" projects to correct bugs
    * still need to implement mailing list
