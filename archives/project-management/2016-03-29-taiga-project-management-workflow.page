# Discussion: Taiga Project Management Workflow

*Note: this was a discussion that occurred in Taiga
[US-146](https://tree.taiga.io/project/snowdrift/us/146), "Mailing lists
replaced with Discourse" and subsequently archived so discussion can focus on
the User Story contents.*
([screenshot](2016-03-29-taiga-project-management-workflow.png))

---

## \#146 Mailing lists replaced with Discourse

Created by Stephen Michel - 29 Mar 2016 10:37

Discourse can replicate 100% of mailing list functionality, including creating
new threads from email. It also adds new functionality like the ability to have
cross-list discussions in one place, a web interface, and the ability to
outsource our spam filtration. Given some work, it can also be made to support
single sign on, reducing the number of accounts a person involved with
Snowdrift needs to create.

---

Stephen Michel - 29 Mar 2016 10:45

@chreekat FWIW, it feels weird to me, just throwing something in the backlog
like this, before we've answered the question of "Should this be done at all?"

For example, let's pretend I made a user story about using Skype instead of
jit.si. Clearly we shouldn't do this, even if we had unlimited resources. So
why is it in our backlog?

I think the easy answer to that question is, "everything goes in the backlog,
and then we'll give it a "definitely will not do" status, so people can see
that we've considered it and decided not to. But if that's the purpose of the
backlog, what's the purpose of issues?

The way I was thinking about it, that seems not to be in line with your
thinking, is that issues are for things like the Skype example, where it's not
clear that we should do it under any circumstances. Once it's clear that this
is a thing worth doing if we had unlimited resources, then it goes in the
backlog (via the convenient "promote to User Story" button on every issue).

---

Bryan Richter - 29 Mar 2016 11:56

I am not completely clear in my own mind on the distinction between issues and
the backlog, but there are some things I can say with certainty. "Use skype
instead of jit.si" is a user story. (Just not a very good one — it's missing
the description of who and why.)

Think of classic examples of things that would be called "issues" in any
setting. I want to ride my bike, but I can't find my helmet. I want to hike a
mountain, but I broke my ankle. I want to get along in society, but I have a
crippling fear of eyebrows.

Another word for "issues" is "problems". User stories, on the other hand, are
solutions. Potential solutions, hypothetical solutions maybe, but always a
solution. "Build a rack next to the front door to hold my bike helmet". "Go to
the ER". (Not sure what solution there is to a fear of eyebrows, so I better
just mark it as an issue for now.)

---

Stephen Michel - 29 Mar 2016 12:14

There are 3 standard types of issues in Taiga: bugs, questions, and
enhancements (and we've added tensions).

In a github/gitlab issue tracker, everything is issues, and issues tagged with
'enhancement' are ~~basically the same thing as~~ user stories.

The distinctions that I see between issues and user stories are:

**They have separate access controls.**

- We can allow anyone to create an issue, while keeping user stories as a
  sacred space for team members only.
- This is how it's currently set up.

**Issues have priority and severity fields; user stories have client vs team
requirements.**

- This is where my impression comes from, that user stories are *requirements*
  (or at least, things we've decided we definitely would do if there's nothing
  else in the queue).

**Issues come with a "rejected" status; user stories come with "archived".**

- Obviously we *can* create a "rejected" status for user stories, but this
  sounds to me like Taiga telling us how it's intended to be used: where a user
  story cannot be rejected.

**Issues have a one-click button to upgrade into User Stories.**

- When you click it, it creates a new user story that has the same exact title
  and description as the issue you upgraded.
- Seems like they are acknowledging that some issues (enhancements, and
  probably bug fixes too) contain the same info as user stories -- ie, they're
  user stories that we're not sure we want to support.

---

Stephen Michel - 29 Mar 2016 12:17

A different top-level way to describe how I'm thinking about them:

- "Enhancements are user stories that we haven't reviewed yet.
- User Stories (capitals) are user stories that we've reviewed, that we agree
  with the *why*, and that we think the *what/how* will accomplish the why.

---


Bryan Richter - 29 Mar 2016 12:52

I'm just gonna poke some holes:

On "sacredness": There is only one time we can realistically say, "We are
actually going to do this," and that is during sprint planning. Even that is on
shaky ground. Every holding place leading up to the sprint board cannot be
considered sacred. That is the antithesis of agile.

It is certainly the case that user stories will originate as 'enhancement'
issues. Being admitted to the Taiga Team requires certain understanding and
responsibility, which we don't want to put up as a barrier to making
suggestions. Who then decides to upgrade a enhancement-issue to a US, and when?

If it's the product owner during prioritization, it would have been immediately
better if the issue had just been a US already. This answer would also require
the product owner to go into the Issues view, which is already a total
distraction to their role (even if there's a nice filter to only see
enhancements). Do you want to go into an extra step and click a bunch of
buttons before getting down to your real job?

No, there is a issue-triage role that can determine severity and other salient
characteristics of issues as they come in. They should be the one to upgrade
enhancement-issues. But what is their criteria? "We agree with the why?" But
that requires product owner input, which takes us to the dead end above. The
bar must be lower: the triage nurse must be reasonably sure it's not a
duplicate of some other US, and that's it. Pop it into the backlog.

If the bar is so low, then, surely any Taiga Team member in any role can clear
that bar. Why create extra work for the triage nurses? Why not just put the
user story directly in the backlog?

---


Stephen Michel - 29 Mar 2016 15:25

First off, I'm going to use "US" to mean Taiga-User-Story where "user story" is
just talking about anything in the form of "who should do what because why."

    I'm just gonna poke some holes:

Holes are great, that's how we figure this out :)

    On "sacredness": There is only one time we can realistically say, "We are
    actually going to do this," and that is during sprint planning. Even that
    is on shaky ground. Every holding place leading up to the sprint board
    cannot be considered sacred. That is the antithesis of agile.

Perhaps sacred was too strong of a word. If we have a US today to switch to
discourse for the reasons listed in this issue's description, but then tomorrow
Discourse changes to a proprietary license, we'll want to re-evaluate when that
US gets high enough in the queue.

I do see your point: that as long as we're re-evaluating all the time, it
doesn't really matter if at some point we decided it'd be a good idea to do X.

Here's my latest thinking: maybe Issues should exist exclusively to **filter user stories that we don't want to implement *and never will*.**

    No, there is a issue-triage role that can determine severity and other
    salient characteristics of issues as they come in. They should be the one
    to upgrade enhancement-issues. But what is their criteria? "We agree with
    the why?" But that requires product owner input, which takes us to the dead
    end above. The bar must be lower: the triage nurse must be reasonably sure
    it's not a duplicate of some other US, and that's it. Pop it into the
    backlog.

    If the bar is so low, then, surely any Taiga Team member in any role can
    clear that bar. Why create extra work for the triage nurses? Why not just
    put the user story directly in the backlog?

Removing duplicates is one thing, and yes, anybody can do that. However, I
think the more important criteria is **whether the user story is in line with
the vision.**

- This is why the Skype story isn't valid.
- Hypothetical: say we were running Discourse and someone proposed a user story
  to switch back to mailmain2. I think this is still a valid US. It might be
  bottom priority, or moved to the archive (which I'm thinking of as kind of
  the equivalent of "rejected" -- a US is never truly rejected, because it's
  always something we *might* do if circumstances change -- as opposed issues,
  which may be rejected if they're out of line with our values)

That's a much higher bar; for that purpose, I think "Visionary" would be a
better title than "Triage Nurse". I actually proposed a "Visionary" role for
this purpose back when we first started doing governance, but others didn't see
the need (in part because our situation was so much less clear, I think) and so
I didn't press the matter, in favor of getting our current stuff set up. Now
I'm actually thinking the PO should do it, though (see below). In part that's
because "visionary" seems like you're setting the vision, which is the Board's
job.

    It is certainly the case that user stories will originate as 'enhancement'
    issues. Being admitted to the Taiga Team requires certain understanding and
    responsibility, which we don't want to put up as a barrier to making
    suggestions. Who then decides to upgrade a enhancement-issue to a US, and
    when?

    If it's the product owner during prioritization, it would have been
    immediately better if the issue had just been a US already. This answer
    would also require the product owner to go into the Issues view, which is
    already a total distraction to their role (even if there's a nice filter to
    only see enhancements). Do you want to go into an extra step and click a
    bunch of buttons before getting down to your real job?

I question that (1) this is part of or during prioritization and (2) that it's not a part of the PO's "real job."

IIUI, it's the PO's job to ensure we're delivering the right product. That
encompasses ensuring the product is useful AND in line with the vision. The
filtering is only related to the second part. It's done in the issues view at a
separate time from prioritization. So even though it might be the same person
doing both, it's not like they have to jump back and forth between views.

Anticipating a possible response: If there's technical debt we absolutely need
to repay, or internal tools that would be helpful to start using, it would be a
big failure of process if the PO vetoed that. I think the best way to do this
is: give PO an accountability of "Rejecting user stories *that aren't in line
with the vision*, and to promoting all other user stories into US." I think
that's a pretty hard accountability to abuse, but if the PO does, it'll causes
tensions and probably will end with the GM removing that partner from the PO
role.

--

Then there's another question of **Who prioritizes the backlog and how?** I
have ideas here and I want to chat with you but this comment is already a
freakin' essay so I'll put that off. *tl;dr I think both the teams and PO
should do this.*

If we wanted, we could split it into PO into two separate jobs, PO for the
prioritizing part and Visionary (pending better title) for the filtering part.
I think that's overkill, but it's possible, if someone senses a tension related
to having them in the same role.

---

Stephen Michel - 29 Mar 2016 15:41

As an aside: I think the process of "Issues might not be in line with our
values, US always are" also lends a huge amount of clarity to the process of
verifying that all the user stories Interaction Design makes for the website
are in line with our values.






