---
title: FLO Development and Economic Psychology
categories: communications
...

## Beyond *Homo economicus*

As in the [Snowdrift Dilemma](snowdrift-dilemma), when we want something done, we may do what we can even if others won't help. This strategy can make sense even when we assume purely rational self-interest. However, we are social creatures, and our decisions involve emotional factors like compassion for others, desire for approval, and concern about the larger impact of our actions, as well as grudge-holding, guilt, and spite.[^evolution] The design of a platform like Snowdrift.coop can affect whether users tend toward a calculated, self-interested approach or a more social, community-focused approach.

[^evolution]: For more on the evolution of human social economics, see <span class="citation">Dawkins, Richard (1976, updated 2006) <i>The Selfish Gene</i>. New York City: Oxford University Press.</span>

### Money can displace social considerations

Most of us are often generous and thoughtful of others, but when money enters the picture, it can *displace* rather than augment our social focus.[^market-vs-social]

[^market-vs-social]:
For a concise summary of many studies about the conflict between social considerations and calculated financial incentives, see pp.190-196 of *Practical Wisdom: The Right Way To Do The Right Thing* by Barry Schwartz and Kenneth Sharpe, New York: Riverhead Books, 2010.

    A more recent study on the tipping point between social and economic motivations suggests that money facilitates cooperation in large, but not small, group settings: Camera, Gabriele, et al, “Money and trust among strangers”, *Proceedings of the National Academy of Sciences*, 26 August 2013. <http://www.pnas.org/content/early/2013/08/21/1301888110>.

![](/assets/nina/MimiEunice_86-640x199.png)

Prices encourage calculated cost-benefit analysis. The perks offered in conventional crowdfunding sites like Kickstarter and IndieGoGo put focus on the economic cost-benefit of pledging, sometimes deemphasizing the social value of doing your part to support a project.^[Of course, most crowdfunding sites are dominated by proprietary projects, so social value is already dubious in *those* cases.]

### External rewards can crowd out intrinsic motivation

![](/assets/nina/MimiEunice_38-640x199.png)

People achieve their best when driven by desire to solve challenges and contribute to the greater good. Providing adequate basic income provides a freedom and autonomy that allows people to focus on their work without distraction. Beyond assuring a comfortable well-being, further focus on pay is generally counterproductive.

### Social considerations can override rational self-interest

People desire gratitude and approval. We may feel good about being generous, but we still appreciate acknowledgement. On the flip side, when we feel exploited, we may choose to suffer ourselves just to punish the freeloader(s). We may hold long-term grudges. In cases of widespread freeloading, we may lose trust in society and become cynical and discouraged.

Consider another well-known game theory scenario: the Ultimatum Game. One of two participants gets to propose a split of $100 with a second participant. If the second participant rejects the offer, both get nothing. Pure economic analysis says that the second participant should accept any non-zero offer. Even a 99 to 1 split still gains the second player $1. In practice, however, participants often reject insulting lowball offers.[^ultimatum] Of course, the first player usually realizes that others may retaliate for unfairness and will tend to offer a more equitable split in the first place.

[^ultimatum]: Tirole, Jean. “Rational irrationality: Some economics of self-management.” *European Economic Review* 46 (2002) 633–655. <http://teaching.ust.hk/~mark329y/EconPsy/Rational%20irrationality-Some%20economics%20of%20self-management.pdf>

    An interesting note: trained economists tend to act with rational self-interest in these games instead of showing the social behaviors of most other participants.

## How effective fundraising considers psychology

**For effective fundraising, we must encourage cooperation and maintain social sensitivity.**

In a "walk-a-thon", for example, the walkers don't do anything to directly help their cause; they merely show donors that others care about the cause as well. When everyone participates, it reduces the psychosocial problems caused by freeloading. The wider and more active the participation, the better the likelihood of maintaining social norms over market norms.

Of course, **activities like walking mean more when financial pledges are proportional to the amount of walking and the distance is not predetermined.** That way, walkers can feel that they really make a difference, and donors feel more inspired by the show of commitment. We also get feelings of accomplishment from having worked hard to achieve a goal.

## How Snowdrift.coop avoids focusing on money

In addition to our [donation-matching setup](mechanism), other aspects of Snowdrift.coop maximize positive social pressures and sense of community. 

### Emphasizing livable salary

Patronage systems operate most effectively when all those involved view one another as individual people with needs and feelings. However, the system may trend towards less effective self-interested strategies when patrons and developers perceive their relationship as a market exchange. So, we encourage a focus on mutual effort to improve resources for everyone while providing workers a reliable livelihood.

To emphasize cooperative social norms, **Snowdrift.coop endorses paying team members fair market salaries for their work without putting hard numbers on billable hours**. We encourage patrons to judge projects on overall monthly progress rather than pricing every detail. Patrons should also give some deference to the decisions and autonomy of project teams. Likewise, we encourage developers to do their best for the good of the project and the appreciation of the patrons. This puts projects in positive [competition](compete) to prove themselves most deserving to potential patrons.

### Encouraging creative contributions

FLO projects often thrive on volunteer input, so we must work to maintain that at the same time as we fund primary project teams.

![](/assets/nina/ME_439_OpenSource-640x199.png)

Some say that time is money, but we cannot effectively quantify and then achieve some sort of matching for volunteer time. Instead, we must appeal to positive social encouragement such as public acknowledgement.

**We will have a "thanks" button specifically for non-monetary input from non-team-members.** Users who have been "thanked" will get listed on project pages and have a badge by their account name elsewhere (such as in discussion forums) marking them as "contributors" (a distinct title from that of "patrons" who provide funding; of course, the two titles are not mutually exclusive).

As we develop, Snowdrift.coop will include further elements to encourage everyone involved to contribute creatively as best as they can to all sorts of projects.
