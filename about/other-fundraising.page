---
title: Fundraising for Charities and Other Purposes
categories: communications
...

Obviously, our [matching pledge mechanism](mechanism) could work for all sorts 
of fundraising. The extended community-wide matching and long-term support 
commitment may achieve superior results.

Among the most prominent traditional fund-drives, consider those from public 
broadcasting networks (such as NPR and PBS in the United States). They usually 
offer matching grants and sustaining pledges as options. Our approach combines 
the best of these existing strategies. We would welcome public broadcasting 
organizations to use Snowdrift.coop *if they will license their productions in 
a free/libre/open way* as would fit our mission and current focus.

**If we succeed initially, we could consider expanding to fund other public 
goods besides non-rivalrous FLO works.** We could include community projects, 
charitable work, and even service and recreational groups. At this point, these 
types of projects are outside the scope of our mission.

Our site's software and all of the writings and associated ideas are available 
for use and adaptation by others within the terms of the licenses we have 
chosen (see the links in the site footer). Otherwise, we have researched and 
listed other sites with other approaches to fundraising and mentioned our 
recommendations, see the [Market Research](/market-research) section for other 
crowdfunding sites of note.
