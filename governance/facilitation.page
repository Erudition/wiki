---
title: Facilitation and Consensus
categories: governance
...

![](/assets/nina/ME_202_SavingTheWorld-640x199.png)

## Facilitated meeting procedure

* Discussions should focus on problem solving, with full and open engagement but limited to the matter at hand.
* For each topic of discussion, first identify and clearly define problems as open-ended questions about how to achieve a certain goal.
* With consensus on a problem definition, collaboratively analyze the problem and develop a broad range of potential solutions.
* Then shift the focus to narrowing the pool of potential solutions through a structured evaluation process based on success criteria determined by the group.
* When a modest number of potentially acceptable solutions remain for consideration, the focus should shift to expanding them into action proposals that can be ranked and discussed in turn until a final decision on a solution is reached by consensus.
* A participant may withdraw a solution that they have suggested from consideration at any time, although another participant could re-introduce it later. 
* The group as a whole may drop a proposal at any time, although it may still be reintroduced later (preferably in an updated form).
* If the group determines that key stakeholders in a decision are inadequately represented, effort must be made to engage the missing group(s), either by delaying a decision or by otherwise engaging their input.


## Consensus

The best decisions come from working toward consensus with all stakeholders included. 

*Consensus* means "general agreement", or, "an answer that everyone can live with, even if they don't love it". When making a group decision based on consensus, you seek "win/win" solutions that actively addresses minority concerns. All stakeholder groups, from team members to users, should at least be represented. Select problem solutions through *reverse* voting: taking the option strongly opposed by the fewest participants, have those who remain opposed list their concerns, and then work to modify the solution to address those concerns.

Coming from a cultural paradigm used to majority rule, it may seem strange to continue discussing a problem after more than half of the group has agreed on the solution they find best. However, a stakeholder group that feels excluded or overridden in the decision-making process may harbor resentment, may sabotage the implementation, and may withdraw from future decision-making.

### With fallback

When sincere attempts to address all minority still fail to reach consenus, a decision should fall back on either a vote of some sort or deference to an appropriate authority role to make the decision. Beyond avoiding deadlock, the possibility of fallback encourages dissenters to compromise on anything not especially serious (as they'd rather their views be addressed somewhat than to be overridden by the fallback). Time spent working toward consensus will still bring up useful ideas that will affect fallback decisions. Dissenters who believe their concerns have at least been considered in good-faith will be less hostile to any ultimate decision.

## Resources

Cultivate.coop has a [guide to practicing consensus](http://cultivate.coop/wiki/Consensus_decision_making) including an extensive list of links. 

[Loomio](https://www.loomio.org/) — A cooperatively-run pay-what-you-want FLO online service for group decision making

[Group Works](http://groupworksdeck.org/) — An open and participatory project to develop a FLO pattern database for facilitation and group process

[Dotmocracy](http://www.iaf-world.org/Libraries/Facilitator_Resource_Centre/Dotmocracy_Handbook.sflb.ashx) — a strategy for facilitators

[Facilitated discovery handbook](http://www.facilitateddiscovery.com/facilitators.html) (seems to mostly be about business settings, though)
